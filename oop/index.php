<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');
require_once('sapi.php');

$sheep = new Animal("shaun");

echo "name: " . $sheep->get_name() . "\n"; // Output: name: shaun 
echo"<br>";
echo "legs: " . $sheep->get_legs() . "\n"; // Output: legs: 4
echo "<br>";
echo "cold blooded: " . $sheep->get_cold_blooded() . "\n"; // Output: cold blooded: no
echo "<br> <br>";

$kodok = new Frog("buduk");
echo "Name: " . $kodok->get_name() . "\n"; // Output: Name: buduk
echo "<br>";
echo "legs: " . $kodok->get_legs() . "\n"; // Output: legs: 4
echo "<br>";
echo "cold blooded: " . $kodok->get_cold_blooded() . "\n"; // Output: cold blooded: no
echo "<br>";
echo "Jump: " . $kodok->jump(); // Output: Jump: Hop Hop
echo "<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->get_name() . "\n"; // Output: Name: kera sakti
echo "<br>";
echo "legs: " . $sungokong->get_legs() . "\n"; // Output: legs: 2
echo "<br>";
echo "cold blooded: " . $sungokong->get_cold_blooded() . "\n"; // Output: cold blooded: no
echo "<br>";
echo "Yell: " . $sungokong->yell(); // Output: Yell : Auooo
echo "<br><br>";

$sapi = new cow ("sapi");
echo "Name: " . $sapi->get_name() . "\n"; // Output: Name: sapi perah
echo "<br>";
echo "legs: " . $sapi->get_legs() . "\n"; // Output: legs: 4
echo "<br>";
echo "cold blooded: " . $sapi->get_cold_blooded() . "\n"; // Output: cold blooded: no
echo "<br>";
echo "Yell: " . $sapi->yell(); // Output: Yell : mow mow
echo "<br>";